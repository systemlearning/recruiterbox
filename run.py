from conway.game import Game
from conway.state import State
from conway.user_input_handler import UserInputHandler

if __name__ == "__main__":
    handler = UserInputHandler()
    grid = handler.input_grid_value()
    new_game = Game(State(grid=grid, width=handler.width, height=handler.height))
    new_game.start()
    print new_game.state
