from conway.exceptions import UserInvalidInput


class UserInputHandler:
    def __init__(self):
        self.height, self.width = self.__user_input("Please enter Width and Height of grid: \n ")
        if self.width <= 1 and self.height <= 1:
            raise UserInvalidInput("We need to work on grid, min value allowed: 2,2")

    def __user_input(self, message):
        data = input(message)
        return data

    def input_grid_value(self):
        print ("Enter grid data")
        text = ""
        for _ in range(self.height):
            line = raw_input()
            if len(line) != self.width:
                raise UserInvalidInput("Invalid width length. It should be equal to {}".format(self.width))
            text += "%s\n" % line
        return text
