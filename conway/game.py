class Game(object):
    def __init__(self, state):

        self.state = state
        self.width = state.width
        self.height = state.height

    def start(self):
        new_board = [[False] * self.width for _ in range(self.height)]

        for column_index, row in enumerate(self.state.board):
            for row_index, cell in enumerate(row):
                neighbours = self.neighbours(row_index, column_index)
                previous_state = self.state.board[column_index][row_index]

                should_live = (neighbours == 3 or (neighbours == 2 and previous_state == True))

                new_board[column_index][row_index] = should_live

        self.state.board = new_board

    def neighbours(self, row_index, column_index):

        count = 0

        for hor in [-1, 0, 1]:
            for ver in [-1, 0, 1]:
                if not hor == ver == 0 and (
                                    0 <= row_index + hor < self.width and 0 <= column_index + ver < self.height):
                    count += self.state.board[(column_index + ver) % self.height][(row_index + hor) % self.width]

        return count