class State(object):
    def __init__(self, grid, width, height):
        live_cells = []

        for row_index, row in enumerate(grid.splitlines()):
            for column_index, cell in enumerate(row.strip()):
                if cell == "*":
                    live_cells.append((row_index + 1, column_index + 1))

        board = [[False] * width for _ in range(height)]

        for cell in live_cells:
            board[cell[0] - 1][cell[1] - 1] = True

        self.board = board
        self.width = width
        self.height = height

    def __repr__(self):
        output = ''
        for column_index, row in enumerate(self.board):
            for row_index, cell in enumerate(row):
                if self.board[column_index][row_index]:
                    output += '*'
                else:
                    output += '.'
            output += '\n'
        return '{}'.format(output)
