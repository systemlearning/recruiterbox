from unittest import TestCase

from conway.state import State


class TestState(TestCase):
    def test_fail_for_grid_values(self):
        grid = "...\n...\n...\n"
        state = State(grid="...\n...\n...\n", height=3, width=3)
        self.assertEqual(state.__repr__(), grid)

    def test_pass_for_state_board(self):
        state = State(grid="...\n...\n...\n", height=3, width=3)
        self.assertEqual(state.board, [[False, False, False], [False, False, False], [False, False, False]])

    def test_pass_for_state_board_with_live(self):
        state = State(grid="...\n.*.\n.*.\n", height=3, width=3)
        self.assertEqual(state.board, [[False, False, False], [False, True, False], [False, True, False]])

    def test_fail_for_state_board(self):
        state = State(grid="...\n...\n...\n", height=3, width=3)
        self.assertNotEqual(state.board, [[False, True, False], [False, False, False], [False, False, False]])
