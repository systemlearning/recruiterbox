from unittest import TestCase

from conway.game import Game
from conway.state import State


class TestGame(TestCase):
    def setUp(self):
        self.state = State(grid="........\n...*....\n...**....\n........\n", height=4, width=8)

    def test_pass_for_correct_grid(self):
        game = Game(self.state)
        self.assertEqual(self.state.board, [[False, False, False, False, False, False, False, False],
                                            [False, False, False, True, False, False, False, False],
                                            [False, False, False, True, True, False, False, False],
                                            [False, False, False, False, False, False, False, False]]
                         )
        game.start()

        self.assertEqual(self.state.board, [[False, False, False, False, False, False, False, False],
                                            [False, False, False, True, True, False, False, False],
                                            [False, False, False, True, True, False, False, False],
                                            [False, False, False, False, False, False, False, False]]
                         )

    def test_fail_for_wrong_output(self):
        game = Game(self.state)
        game.start()

        self.assertNotEqual(self.state.board, [[False, False, False, False, False, False, False, False],
                                            [False, False, False, False, True, False, False, False],
                                            [False, False, False, True, True, False, False, False],
                                            [False, False, False, False, False, False, False, False]]
                         )
